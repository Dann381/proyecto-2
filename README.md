==== Proyecto 2 ====

Este proyecto consta de una serie de funciones orientadas al análisis de un Dataset que contiene una serie de datos relacionados a la dieta de una lista de países, y cifras de los mismos países respecto a su estado frente a la pandemia producida por el Covid-19.
Se han propuesto cuatro hipótesis o interrogantes a responder antes de analizar el Dataset, para luego poder darles una respuesta respecto a los resultados obtenidos. Estas interrogantes son:

1.- ¿La obesidad es un factor que aumente el riesgo de mortandad por Covid-19?

2.- ¿Qué patrón de alimentación tienen los países con más recuperados por Covid-19?

3.- ¿La cantidad de gente que habita en un país incide en la cantidad de casos confirmados por Covid-19?

4.- ¿El porcentaje de bebidas alcohólicas que se consumen por país incide en la cantidad de casos confirmados por Covid-19?

La respuesta a estas interrogantes se presentan a modo de conclusión dentro del mismo proyecto, y en el caso de la interrogante número 3, se incluye una imágen de formato .png con gráficos para visualizar de mejor manera la conclusión.


==== Pre-Requisitos y Ejecución del Programa ====

>Tener instalado Python 3

>Tener instalado el repositorio Pandas

Para ejecutar este proyecto se debe abrir con Python 3. Una vez abierto, el algoritmo mostrará las hipótesis descritas anteriormente, y le pedirá al usario que ingrese un número del 1 al 4, que representan las interrogantes que se pueden mostrar en la pantalla. Una vez que el usuario ingrese un número válido, el algoritmo mostrará en pantalla un análisis de los datos relacionados a la interrogante seleccionada, y se mostrará además una conclusión basada en los datos obtenidos. En el caso de que el usuario seleccione la interrogante número 3, además de mostrarse una conclusión, el algoritmo arrojará una imagen en formato .png con dos gráficos, cuyo propósito es demostrar de manera visual lo expresado en la conclusión, destacando la diferencia entre los datos de los países analizados. Esta imágen se guardará en el directorio en donde se encuentre el algoritmo, y se puede acceder a ella presionando con doble click en la misma imágen.


==== Funcionamiento ====

Para ver el funcionamiento del programa, se debe abrir el archivo con cualquier edito de texto, en mi caso ocupé VIM para editarlo.
Lo primero que se encontrará en el programa, son las librerías utilizadas. Luego de esto encontraremos tres funciones pequeñas. La primera, llamada "cargar_dataset", lo que hace es abrir el Dataset y lo guarda en la variable "data". La segunda función, llamada "limpieza_dataset", elimina todos los países que contengan datos nulos en sus columnas, debido a que no podemos hacer un análisis de un país que tiene datos faltantes. La tercera función, llamada "obtener_columnas", es una función muy importante ya que por aquí se filtran todas las columnas que se van a ocupar para responder cada interrogante. Por ejemplo, en la interrogante número 3 necesitamos analizar la cantidad de casos activos con Covid-19, y el porcentaje de gente que habita cada país, por lo que el Dataset pasa por esta función eliminando todas las demás columnas que no se utilizarán en esta interrogante.
Luego de esto, vienen cuatro funciones únicas, las cuales están adaptadas para responder una interrogante. Por ejemplo, la función "casos_alcohol" está solamente orientada a procesar datos relacionados a la pregunta número 3, además de que presenta una conclusión relacionada solo a los datos obtenidos para esa pregunta.
Finalmente, viene dos funciones más, la primera es "menu", la cual presenta una descripción de las opciones que puede escribir el usuario. La segunda es "main", que es la función principal, y en ella se llaman a todas las demás funciones. Tecnicamente hablando es la columna vertebral del código. En esta función existe un bucle que sirve para solo detenerse cuando el usuario ingrese la palabra "salir", lo que permite que el código siga funcionando hasta que el usuario desee terminar el programa.


==== CONSTRUIDO CON ====

Vim - the ubiquitous text editor


==== Desarrollo ====

Este programa fue desarrollado por Daniel Tobar Hormazábal