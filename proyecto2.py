import os
import pandas as pd
import matplotlib.pyplot as plt


def cargar_dataset():
    data = pd.read_csv("suministro_alimentos_kcal.csv")
    return data


def limpieza_dataset(data):
    data = data.dropna()
    return data


def obtener_columnas(columnas, data):
    columnas = data.iloc[:, columnas]
    return columnas


def casos_alcohol(alcohol):
    masalcohol = []
    etanol = []
    contagios = []
    etanol = alcohol['Alcoholic Beverages'].tolist()
    contagios = alcohol['Active'].tolist()
    mayorcontagios = max(contagios)
    menorcontagios = min(contagios)
    print(f"El mayor porcentaje de casos de todos los países es"
          f" {mayorcontagios}")
    print("El menor porcentaje de casos de todos los países es"
          f" {menorcontagios}")
    for i in range(3):
        maximo = max(etanol)
        temp = etanol.index(maximo)
        masalcohol.append(temp)
        etanol[temp] = 0
    print("\nLos países con mayor consumo de alcohol son:")
    for indice in masalcohol:
        print(alcohol.iloc[indice][0], "--> Ingesta del alcohol:",
              alcohol.iloc[indice][1], "|| Casos activos:",
              alcohol.iloc[indice][2], "\n")
    print("\nConclusión:")
    print("Como se puede observar, los países con un mayor consumo de alcoho"
          "l no presentan un porcentaje de casos activos elevados, incluso s"
          "e podría decir que sus casos activos se han mantenido bajos, por "
          "lo que se puede inferir que un mayor consumo de alcohol no agrava n"
          "i aumenta la cantidad de casos activos en un país. Es por lo anteri"
          "or que se concluye que la cantidad de alcohol ingerida por un país "
          "no tiene mayor influencia en la cantidad de casos confirmados por C"
          "ovid-19.\n")


def casos_gente(gente):
    masgente = []
    menosgente = []
    population = []
    population = gente['Population'].tolist()
    for i in range(2):
        maximo = max(population)
        minimo = min(population)
        temp = population.index(maximo)
        tempo = population.index(minimo)
        masgente.append(temp)
        menosgente.append(tempo)
        population[temp] = 10108000
        population[tempo] = 10108000
    print("Los países con más población son:")
    for indice in masgente:
        print(gente.iloc[indice][0], ": Casos activos:",
              gente.iloc[indice][1], "Población:",
              gente.iloc[indice][2], "\n")
    print("\nLos países con menos población son:")
    for indice in menosgente:
        print(gente.iloc[indice][0], ": Casos activos:",
              gente.iloc[indice][1], "Población:",
              gente.iloc[indice][2], "\n")
    comparacion_extremos = gente[(gente["Country"] == "China")
                                 | (gente["Country"] == "Dominica")]

    comparacion_extremos.plot(kind="bar", x="Country", subplots=True,
                              figsize=(14, 6), sharex=False)
    plt.savefig("coomparacion.png")
    print("Conclusión:")
    print("Como podemos observar, China es uno de los países que más poblac"
          "ión tiene, aún así, la cantidad de casos que posee es muy baja, "
          "aproximadamente 0.0000374 % de su población ha tenido o tiene Co"
          "vid-19, en cambio Dominica y San Vicente y las Granadinas son lo"
          "s países con menor población del Dataset, aún así, tiene muchos "
          "mas contagiados que China, incluso San Vicente y las Granadinas "
          "tiene un porcentaje de contagiados similar a la India, quien tie"
          "ne 10000 veces mas población. De lo anterior podemos concluir qu"
          "e la cantidad de gente que habita un pais no incide en la cantid"
          "ad de contagiados por Covid-19. Algunos factores que si podrían "
          "afectar son la densidad poblacional, o las medidas provisorias t"
          "omadas para enfrentar la pandemia, pero eso no lo podemos afirma"
          "r con los datos que tenemos.")
    print("En el gráfico se muestra una comparación entre el porcentaje de "
          "casos activos y la población de China y Domina, donde se puede o"
          "bservar lo anteriormente descrito.")
    print("\n")


def alimentacion_recuperados(recuperados):
    massanos = []
    indices = []
    massanos = recuperados['Recovered'].tolist()
    for i in range(3):
        maximo = max(massanos)
        temp = massanos.index(maximo)
        indices.append(temp)
        massanos[temp] = 0
    print("Los países con más recuperados del Covid-19 son:\n")
    for indice in indices:
        print(recuperados.iloc[indice][0], "--> Recuperados:",
              recuperados.iloc[indice][1],
              "\nIngesta Cereales:", recuperados.iloc[indice][2],
              "\nIngesta Productos Vegetales:", recuperados.iloc[indice][3],
              "\nIngesta Frutas:", recuperados.iloc[indice][4],
              "\nIngesta Productos Animales:", recuperados.iloc[indice][5],
              "\n")
    print("Conclusión:")
    print("Los países mostrados anteriormente tienen la mayor cantidad de rec"
          "uperados del Covid-19, pero como podemos observar, Israel tiene da"
          "tos erróneos, ya que posee mas de 15000 % de ingesta de Granos (ce"
          "reales), y Perú también posee datos erróneos, ya que posee mas de "
          "3000 % de ingesta de Frutas, por lo que los excluiremos para hacer"
          " el análisis.\nLos datos óptimos de ingesta diaria de alimentos se"
          "gún la USDA son: 30 % de granos, 40 % de verduras, 10 % de frutas "
          "y 20 % de proteı́na. Los datos de ingesta diaria de alimentos de Pa"
          "namá son aproximadamente: Cereales (granos): 21 %, Productos veget"
          "ales (verduras): 40 %, Frutas: 2 % y Productos animales (proteínas"
          "): 11 %. Si bien hay algunos alimentos que no se están consumiendo"
          " según lo que propone la USDA, los valores son bastante cercanos, "
          "por lo que podemos decir que los países que mejor han podido enfre"
          "ntar la pandemia tiene una dieta similar a la propuesta por la USD"
          "A.")


def obesidad_covid(obesidad):
    obesos = []
    indices = []
    obesos = obesidad['Obesity'].tolist()
    muertes = obesidad['Deaths'].tolist()
    maxmuertes = max(muertes)
    posicion = muertes.index(maxmuertes)
    print("El país con más muertes por Covid-19 es:",
          obesidad.iloc[posicion][0], "\nPorcentaje de muertos:",
          obesidad.iloc[posicion][2], "\nPorcentaje de obesidad:",
          obesidad.iloc[posicion][1], "\n")
    for i in range(5):
        maximo = max(obesos)
        temp = obesos.index(maximo)
        indices.append(temp)
        obesos[temp] = 0
    print("Los países con mayor porcentaje de obesidad son:")
    for indice in indices:
        print(obesidad.iloc[indice][0],
              "\nPorcentaje obesidad:", obesidad.iloc[indice][1],
              "\nPorcentaje muertes por Covid-19:", obesidad.iloc[indice][2],
              "\n")
    print("Conclusión:")
    print("Como podemos observar, el país con mas porcentaje de muertos por "
          "Covid-19 no posee un porcentaje muy alto de obesidad. En cambio, "
          "los países con mas porcentaje de obesidad no tienen un porcentaje"
          " muy elevado de muertes por Covid-19, por lo que podemos concluir"
          " que, si bien la obesidad podría traer un riesgo mayor al contrae"
          "r Covid-19 en casos particulares, en realidad en terminos de pobl"
          "ación no es un factor que afecte significativamente, o tal vez po"
          "dría ser un indicador de que los países que tienen mejor preparac"
          "ción para afrontar la pandemia, tienden a ser mas obesos en termi"
          "nos de porcentaje.\n")


def menu():
    os.system("clear")
    print("<<<<<<<<<<<<<<< Análisis de Dataset >>>>>>>>>>>>>>>\n")
    print("Hipotesis a responder:")
    print("¿La obesidad es un factor que aumente el riesgo de mortandad por"
          " Covid-19? (1)")
    print("¿Qué patrón de alimentación tienen los países con más recuperado"
          "s por Covid-19? (2)")
    print("¿La cantidad de gente que habita en un país incide en la cantidad d"
          "e casos confirmados por Covid-19? (3)")
    print("¿El porcentaje de bebidas alcohólicas que se consumen por país inc"
          "ide en la cantidad de casos confirmados por Covid-19? (4)")


def main():
    opcion = "0"
    data = cargar_dataset()
    data = limpieza_dataset(data)
    obesidad = obtener_columnas([0, 24, 27], data)
    recuperados = obtener_columnas([0, 28, 5, 21, 8, 2], data)
    gente = obtener_columnas([0, 29, 30], data)
    alcohol = obtener_columnas([0, 1, 29], data)
    while True:
        menu()
        print("Ingrese un número para ver los resultados")
        print("Para salir, escriba 'salir'")
        opcion = input("Opción: ")
        os.system('clear')
        if opcion == '1':
            obesidad_covid(obesidad)
        elif opcion == '2':
            alimentacion_recuperados(recuperados)
        elif opcion == '3':
            casos_gente(gente)
        elif opcion == '4':
            casos_alcohol(alcohol)
        elif opcion.upper() == 'SALIR':
            print("Programa terminado correctamente")
            break
        else:
            os.system('clear')
            print("Ingrese un número válido\n")
        aux = input("Presione enter para continuar...")


if __name__ == "__main__":
    main()
